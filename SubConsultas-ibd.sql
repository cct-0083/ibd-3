-- Subconsultas

drop table if exists funcionario;
create table funcionario (
	id serial,
	nome varchar(30),
	depto varchar(30),
	primary key (id)
)

alter table funcionario add column salario decimal;

insert into funcionario (nome, depto, salario) values
('Alexandre','RH',5610.10),
('Rosana','RH',6315.10),
('Carlos','RH',1500.00),
('Lorena','TI',7815.15),
('Robson','RH',10650.00),
('João','TI',3700.00),
('Maria','Vendas',5700.00),
('Rubens','Vendas',9350.70),
('Mariano','Vendas',5600.00),
('Jobson','TI',9700.00),
('Carine','SAC',3700.00),
('Carla','SAC',15870.00),
('Núbia','SAC',1200.00),
('Carlson','RH',8500.00),
('Manoel','TI',17850.00),
('Lorena','SAC',1200.00),
('Eduardo','RH',800.00);

-- Série de consultas com subconsultas
-- Selecione uma pessoa que trabalha no departamento de TI e SAC.
Select f.nome
from funcionario f
where (f.depto = 'SAC') and (f.nome in (Select f.nome 
			   from funcionario f
			   where f.depto='TI'))

-- Selecione um funcionario do RH que ganha mais do que qualquer do departamento de Vendas
Select f.nome
from funcionario f
where (f.depto = 'RH') and (f.salario > ANY (
							Select salario
                            from funcionario
                            where depto='Vendas'))		
	
-- Selecione um funcionario de outros departamentos que ganha igual a qualquer do SAC
Select f.nome, f.depto
from funcionario f
where (f.depto <> 'SAC') and (f.salario = ANY (
                              Select salario
							  from funcionario 
							  where depto='SAC'))
	
-- Selecione um funcionário da TI que ganha mais do que todos do departamento SAC
Select f.nome
from funcionario f
where (f.depto = 'TI') and (f.salario > ALL (
							Select salario 
                            from funcionario 
                            where depto='SAC'))			

-- Subconsulta para verificar se alguem da mais do que o maior salário do RH
-- Observação: a subconsulta deve retorna somente um elemento
Select f.nome
from funcionario f
where (f.depto = 'TI') and (f.salario > (Select max(salario)
										 from funcionario
										 where depto='RH'))
										 
-- Verificar se alguem da TI ganha menos do que o menor salário  do SAC
Select f.nome
from funcionario f
where (f.depto = 'TI') and (f.salario < (Select min(salario)
										 from funcionario
										 where depto='SAC'))

-- Verificar se alguem do RH ganha menos do que o menor salário de Vendas
Select f.nome
from funcionario f
where (f.depto = 'RH') and (f.salario < (Select min(salario)
										 from funcionario
										 where depto='Vendas'))

-- Mostrar os maiores salários de cada depto
select max(f.salario)
from funcionario f
where f.depto='SAC'
union
select max(f.salario)
from funcionario f
where f.depto='Vendas'
union
select max(f.salario)
from funcionario f
where f.depto='TI'

										 
										 
										 