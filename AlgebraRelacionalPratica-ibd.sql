 create table comprador (
   codigo serial,
   cpf char(11),
   nome varchar(30),
   cidade varchar(30),
   primary key (codigo)	
 );

 create table vendedor (
   codigo serial,
   cpf char(11),
   nome varchar(30),
   cidade varchar(30),
   primary key (codigo)
 );

 -- Povoando as tabelas
 -- Primeiro a tabela comprador
 insert into comprador(cpf,nome,cidade) values ('11111111111','Ana','Planaltina'),('22222222222','Ricardo','Plano Piloto'),
 ('33333333333','Luis','Gama'),('44444444444','Zenaide','Sobradinho'),('55555555555','Bruna','Recanto das Emas'),
 ('10101010101','Carine','Samambaia'),('12345678910','Larissa','Riacho Fundo');

 -- Povoar a tabela vendedor

insert into vendedor(cpf,nome,cidade) values ('08080808080','Ana','Planaltina'),('66666666666','Roberto','Planaltina'),
 ('00011100011','Carlos','Plano Piloto'),('77777777777','Zenaide','Sobradinho'),('55555555555','Bruna','Recanto das Emas'),
 ('33333333333','Luis','Gama');

-- Operação de projeção (evita duplicatas -- somente na algebra)
-- Selecionando os compradores
select * from comprador 

-- Selecionando os vendodores
select * from vendedor

-- Selecionando as cidades dos vendedores
select cidade from vendedor

-- Selecionando as cidades como é feito na algebra
select distinct cidade from vendedor

-- Operação de seleção na algebra - cláusula where do SQL
select * from vendedor
where (cidade='Planaltina')

-- Produto cartesiano
select comprador.nome,vendedor.nome -- Projeção
from comprador, vendedor -- Produto cartesiano
where (comprador.cidade = vendedor.cidade) -- Seleção

-- Otimizacao de consultas
-- Exemplo de consulta não otimizada
select vendedor.nome
from vendedor, comprador
where (vendedor.cpf = comprador.cpf)

-- Exemplo da mesma consulta otimizada
-- Antecipar projecões ou seleções e manter expressões únicas
select vendedor.nome
from (select nome,cpf from vendedor) as vendedor,(select cpf from comprador) as comprador
where (vendedor.cpf = comprador.cpf)

-- Operação de União
-- Mesmo dominio e mesma quantidade de tuplas
-- A operação evita duplicatas
select nome from comprador union select nome from vendedor

-- Union all mostra todos os valores (não condiz com a algebra)
select nome from comprador union all select nome from vendedor

-- Operação de diferença
-- No postgres except representa diferença da algebra
select nome from comprador except select nome from vendedor

-- Operação de interseção
select nome from comprador intersect select nome from vendedor

-- Junção 
-- Mandatório ter uma condição que retorna uma combinação
select vendedor.nome, comprador.nome
from vendedor, comprador
where (vendedor.cpf = comprador.cpf)

-- É a mesma consulta usando junção
select vendedor.nome, comprador.nome
from vendedor join comprador on (vendedor.cpf=comprador.cpf)

-- No decorrer do tempo esta consulta tem melhor performance (quando há muitos dados)

-- Junção natural (junção que compara em duas relações os campos que possuem o mesmo nome
-- O predicado é a igualdade dos valores dos atributos
select *
from vendedor natural join comprador

-- A tabela vendedor possui os mesmos atributos de comprador 
-- Na junção natural o predicado da junção é a igualdade entre todos os atributos com mesmo nome

-- Transformando em junção comum teriamos o equivalente
select *
from vendedor v join comprador c on (v.codigo = c.codigo and v.nome = c.nome and v.cidade = c.cidade and v.cpf = c.cpf)

-- Com a única diferença que a junção natural não apresenta atributos repetidos no resultado

-- Junções externas
-- Equivalem a uma junção interna com elementos que só estão a esquerda, direita ou os dois.
-- Exemplos
-- Junção à esquerda
select vendedor.nome, comprador.nome
from vendedor left join comprador on (vendedor.cpf = comprador.cpf)
-- Junção à direita
select vendedor.nome, comprador.nome
from vendedor right join comprador on (vendedor.cpf = comprador.cpf)
-- Junção completa (à esquerda e à direita)
select vendedor.nome, comprador.nome
from vendedor full join comprador on (vendedor.cpf = comprador.cpf)

