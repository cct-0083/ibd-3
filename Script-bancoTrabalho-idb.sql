create table cliente (
	codigo int,
	nome varchar(30) not null,
    cidade varchar(30) not null,
	constraint "cliente_codigo_pk" primary key (codigo)
)

create table projeto (
	codigo int,
	descricao varchar(30) not null,
	constraint "projeto_codigo_pk" primary key (codigo)
)

create table colaborador (
	codigo int,
	nome varchar(30) not null,
	depto varchar(30) not null constraint "colaborador_depto_check" check (depto in ('TI','RH','SAC')),
	cidade varchar(30) not null,
	constraint "colaborador_codigo_pk" primary key (codigo)
)

create table trabalho (
	cod_col int,
	cod_proj int,
	data date not null,
	pagamento numeric(10,2) not null constraint "trabalho_pagamento_check" check (pagamento > 0.00),
	constraint "trabalho_col_proj_pk" primary key (cod_col,cod_proj),
	constraint "trabalho_cod_col_fk" foreign key (cod_col) references colaborador,
	constraint "trabalho_cod_proj_fk" foreign key (cod_proj) references projeto
)

-- Create index no colaborador de depto
create index colaborador_depto on colaborador(depto);

-- Create unique index para projeto
create unique index projeto_descricao on projeto(descricao);

-- Create unique index para colaborador
create unique index colaborador_nome on colaborador(nome);

insert into cliente values (1,'Luciano','Brasilia'),(2,'Carla','Recife'),(3,'Humberto','Bahia'),(4,'Antunes','Brasilia');

insert into projeto values (1,'Portal-Web'),(2,'Vendas-Online'),(3,'Startup-Um'),(4,'Credito');

insert into colaborador values(1,'Luciano','TI','Brasilia'),(2,'Carla','SAC','Recife'),
(3,'Humberto','SAC','Bahia'),(4,'Antunes','RH','Brasilia'),(5,'Lorena','SAC','Goiania'),(6,'Tales','TI','Belem'),
(7,'Tiago','SAC','Bahia'),(8,'Bruno','RH','Goiania'),(9,'Henrique','TI','Brasilia'),(10,'Jorge','RH','Belem'),
(11,'Rita','RH','Sergipe'),(12,'Cinthia','SAC','Londrina'),(13,'Ricardo','TI','Belo Horizonte');

insert into trabalho values(1,2,'10/02/2018',3400.00),(12,4,'15/03/2017',6500.70),
(2,3,'01/02/2019',550.00),(8,2,'03/08/2018',5255.75),(4,4,'05/10/2018',2500.00),
(7,1,'10/03/2019',10835.98),(5,3,'02/01/2019',7930.00),(11,4,'02/03/2019',4659.37),
(1,3,'05/02/2018',5980.30),(6,4,'09/03/2019',14380.00),(10,4,'01/12/2017',8000.00),
(9,3,'05/06/2018',20739.00),(12,2,'03/09/2018',6450.29),(3,3,'09/11/2018',9500.00),
(3,4,'07/02/2019',10500.30),(2,2,'01/04/2019',5930.37),(2,4,'03/07/2018',9000.00),
(1,1,'09/09/2017',11000.00),(6,1,'15/10/2017',3500.00),(7,3,'20/11/2018',4500.00);